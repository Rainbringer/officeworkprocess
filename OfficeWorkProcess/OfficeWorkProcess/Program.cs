﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using OfficeWorkProcess.Workers;

namespace OfficeWorkProcess
{
   
    class Program
    {
        public enum WorkerType
        {
            Director = 0,
            Accountant = 1,
            Designer = 2,
            Manager = 3,
            Programmer = 4,
            Tester = 5,
            Freelancer = 6
        }

        private static int workersCount;

        static void Main(string[] args)
        {
            int min = 10;
            int max = 100;
            workersCount = RandomNumber(min, max);

           var listWorkers = SetWorkers(workersCount);
            Director.SetTask(listWorkers);
           
            //Accountant.CreateReport();
        }

        private static List<Worker> SetWorkers(int workersCount)
        {
            List<Worker> listWorkers = new List<Worker>();
            listWorkers.Add(new Director());
            listWorkers.Add(new Manager());
            listWorkers.Add(new Accountant());

            for (int i = 0; i < workersCount - 3; i++)
            {
                var number = RandomNumber(2, 5);
                switch (number)
                {
                    case (int)WorkerType.Manager:
                        listWorkers.Add(new Manager());
                        break;
                    case (int)WorkerType.Designer:
                        listWorkers.Add(new Designer());
                        break;
                    case (int)WorkerType.Tester:
                        listWorkers.Add(new Tester());
                        break;
                    case (int)WorkerType.Programmer:
                        listWorkers.Add(new Programmer());
                        break;
                }
            }
            return listWorkers;
        }

        private static int RandomNumber(int min, int max)
        {
            Random random = new Random();

            return random.Next(min, max);
        }
    }
}
