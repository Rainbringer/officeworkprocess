﻿namespace OfficeWorkProcess.Workers
{
    public abstract class Worker : IWorker
    {
        protected Worker()
        {
            hours_week = 40;
        }
        private int hours_week;

        public int Hours_week
        {
            get { return hours_week; }
            set { hours_week = value; }
        }

        public bool IsBusy()
        {
            throw new System.NotImplementedException();
        }
        
        public abstract void GetTask();

    }
}