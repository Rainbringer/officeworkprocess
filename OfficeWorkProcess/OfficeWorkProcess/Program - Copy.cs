﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeWorkProcess
{
    class ProgramCopy
    {
        private static int workersCount;
        private static int minValue = 10;
        private static int maxValue = 100;

        static void MainCopy(string[] args)
        {
            Console.WriteLine("Введите количество сотрудников (min 10; max 100)");

            if (CheckInput() && CheckRange())
            {
                var t = workersCount;
            }
        }
        /// <summary>
        /// Check if entered value is number
        /// </summary>
        /// <returns></returns>
        private static bool CheckInput()
        {
            while (!int.TryParse(Console.ReadLine(), out workersCount))
                Console.WriteLine("Ошибка! Некоректный ввод!");
            return true;
        }
        /// <summary>
        /// Check if entered value is not out of range
        /// </summary>
        /// <returns></returns>
        private static bool CheckRange()
        {
            if (workersCount < minValue || workersCount > maxValue)
            {
                do
                {
                    Console.WriteLine("Указанное число не содержится в указанном диапазоне чисел!");
                    workersCount = int.Parse(Console.ReadLine());

                } while (workersCount < minValue || workersCount > maxValue);

            }
            return true;
        }

    }
}
